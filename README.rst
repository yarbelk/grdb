grdb
====


Implementation of a command line 'unix' like rdb system built around TSV files.

Its an implementation based on some documentation from rand corp, https://compbio.soe.ucsc.edu/rdb/

The idea is simple: a collection of command line utilities that you pipe things between.

.. code::

   cat input.tsv | rows Name eq "John Doe" | columns Name Birthday Address | jointbl col.Birthday=col.Holidays main_table

Docs
====

In the process of moving to building man pages via pandocs.  See the bin dirs for the markdown equiv.  the `docs` directly is going to go away as i get this all on line and packages built for different distros.

Approach
========

Get somthing useful implemented for each of the operators in https://compbio.soe.ucsc.edu/rdb/operators.html

Then get the utility functions implemented (`etbl`, `headchg`)


