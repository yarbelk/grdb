package table

type TableOptions struct {
	delim rune
}

// DefaultDelimeter to use
const DefaultDelimeter = "\t"

// Option is a mutator that sets something and returns an option to revert it
type Option func(*TableOptions) Option

func Delimiter(r rune) Option {
	return func(to *TableOptions) Option {
		revertRune := to.delim
		to.delim = r
		return Delimiter(revertRune)
	}
}
