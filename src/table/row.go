package table

import (
	"encoding/csv"
	"fmt"
	"io"

	"gitlab.com/yarbelk/grdb/src/errors"
)

// Column is a single column definition
type Column struct {
	Name, Definition string
}

type Reader interface {
	Read() ([]string, error)
}

type Writer interface {
	Write([]string) error
}

type Columns []Column

// Index of the requested name, or -1
func (c Columns) Index(name string) int {
	for i := 0; i < len(c); i++ {
		if c[i].Name == name {
			return i
		}
	}
	return -1
}

// WriteHeader writes out the header to the provided csv.Writer
func (t *Table) WriteHeader(output Writer) error {
	names := make([]string, 0, len(t.Header))
	definitions := make([]string, 0, len(t.Header))
	for _, c := range t.Header {
		names = append(names, c.Name)
		definitions = append(definitions, c.Definition)
	}
	if err := output.Write(names); err != nil {
		return err
	}
	if err := output.Write(definitions); err != nil {
		return err
	}
	return nil
}

// Table is a combination of a definition and the actions that can be performed
// on a stream
type Table struct {
	Header Columns
	reader Reader

	options *TableOptions
}

func (t *Table) Read() ([]string, error) {
	return t.reader.Read()
}

// New probably needs an options thing (options...)
func New(in io.Reader, opts ...Option) (*Table, error) {
	options := new(TableOptions)
	for _, opt := range opts {
		// ignore revert here
		opt(options)
	}

	csvReader := csv.NewReader(in)
	csvReader.Comma = options.delim
	table := &Table{
		Header:  make([]Column, 0),
		reader:  csvReader,
		options: options,
	}
	names, err := table.reader.Read()
	if err != nil {
		return nil, err
	}
	definitions, err := table.reader.Read()
	if err != nil {
		return nil, err
	}

	for i, name := range names {
		table.Header = append(table.Header, Column{Name: name, Definition: definitions[i]})
	}
	return table, nil
}

// Row is a row of strings; the raw data from the file
type Row []string

// Query takes 3 arguments; NAME operator Value
func (t Table) Query(q []string) (func(r Row) bool, error) {

	if len(q) != 3 {
		return nil, fmt.Errorf("Need 3 arguments, got %d: %v, %w", len(q), q, errors.WrongArgumentNumber)
	}
	name, operator, value := q[0], q[1], q[2]

	idx := t.Header.Index(name)
	if idx == -1 {
		return nil, errors.MissingColumn
	}
	switch operator {
	case "eq":
		return func(r Row) bool {
			return r[idx] == value
		}, nil
	case "lt":
		return func(r Row) bool {
			return r[idx] < value
		}, nil
	default:
		return nil, fmt.Errorf("%w: %s", errors.InvalidOperator, operator)
	}
}

// FilterNames returns a new column with just the names
// from
func (r Row) FilterNames(names ...string) Row {
	return Row{}
}
