package table_test

import (
	"encoding/csv"
	"fmt"
	"strings"
	"testing"

	"gitlab.com/yarbelk/grdb/src/errors"
	"gitlab.com/yarbelk/grdb/src/table"
)

func basicFixture() (table.Table, table.Row) {
	table := table.Table{Header: table.Columns{
		table.Column{Name: "NAME", Definition: "6"},
		table.Column{Name: "COUNT", Definition: "5N"},
		table.Column{Name: "TYP", Definition: "3"},
		table.Column{Name: "AMT", Definition: "5"},
		table.Column{Name: "OTHER", Definition: "8"},
		table.Column{Name: "RIGHT", Definition: "8>"},
	}}
	return table, []string{"Bush", "44", "A", "113", "Another", "This"}

}

func TestEqualityOnRow(t *testing.T) {
	tbl, rowData := basicFixture()
	testData := []struct {
		row      table.Row
		query    []string
		response bool
	}{
		{
			row:      rowData,
			query:    []string{"NAME", "eq", "Bush"},
			response: true,
		},
		{
			row:      rowData,
			query:    []string{"NAME", "eq", "bush"},
			response: false,
		},
		{
			row:      rowData,
			query:    []string{"COUNT", "eq", "44"},
			response: true,
		},
		{
			row:      rowData,
			query:    []string{"COUNT", "eq", "43"},
			response: false,
		},
	}

	for _, td := range testData {
		t.Run("Basic equality tests on multiple columns", func(t *testing.T) {
			query, err := tbl.Query(td.query)
			if err != nil {
				t.Logf("Expected no error from query, got %v", err)
			}
			match := query(td.row)
			if err != nil {
				t.Logf("Expected no error, got %v", err)
			}
			if match != td.response {

				t.Logf("Expected %+v. got not that", td)
				t.Fail()
			}

		})
	}
}

func TestType20Error(t *testing.T) {
	tbl, rowData := basicFixture()
	testData := []struct {
		row      table.Row
		query    []string
		response error
	}{
		{
			row:      rowData,
			query:    []string{"Name", "eq", "Bush"},
			response: errors.MissingColumn,
		},
		{
			row:      rowData,
			query:    []string{"george", "eq", "bush"},
			response: errors.MissingColumn,
		},
	}

	for _, td := range testData {
		t.Run("Basic equality tests on multiple columns", func(t *testing.T) {
			_, err := tbl.Query(td.query)
			if err != td.response {
				t.Logf("Expected error from query, got '%+v'", err)
				t.Log(td.query)
				t.Log(td.row)
				t.FailNow()
			}
		})
	}

}

func TestReadHeader(t *testing.T) {
	expected := table.Table{
		Header: table.Columns{
			{Name: "NAME", Definition: "6"},
			{Name: "COUNT", Definition: "5N"},
			{Name: "TYP", Definition: "3"},
			{Name: "AMT", Definition: "5"},
			{Name: "OTHER", Definition: "8"},
			{Name: "RIGHT", Definition: "8>"},
		},
	}

	input := strings.NewReader("NAME\tCOUNT\tTYP\tAMT\tOTHER\tRIGHT\n6\t5N\t3\t5\t8\t8>")
	tbl, err := table.New(input, table.Delimiter('\t'))

	if err != nil {
		t.Logf("Unexpected error %v", err)
		t.FailNow()
	}

	if len(tbl.Header) != len(expected.Header) {
		t.Logf("Parsed and expected have different lengths parsed:\n%v\nexpected:\n%v\n", tbl, expected)
		t.FailNow()
	}

	for i, c := range tbl.Header {
		if c.Name != expected.Header[i].Name || c.Definition != expected.Header[i].Definition {
			t.Logf("Element %d: expected <> parsed: %+v <> %+v", i, expected.Header[i], c)
			t.Fail()
		}
	}
}

func TestWriteHeader(t *testing.T) {
	testData := []struct {
		delim    rune
		expected string
	}{
		{
			delim:    '\t',
			expected: "NAME\tCOUNT\tTYP\tAMT\tOTHER\tRIGHT\n6\t5N\t3\t5\t8\t8>\n",
		},
		{
			delim:    ',',
			expected: "NAME,COUNT,TYP,AMT,OTHER,RIGHT\n6,5N,3,5,8,8>\n",
		},
		{
			delim:    '♥',
			expected: "NAME♥COUNT♥TYP♥AMT♥OTHER♥RIGHT\n6♥5N♥3♥5♥8♥8>\n",
		},
	}

	input := table.Table{
		Header: table.Columns{
			{Name: "NAME", Definition: "6"},
			{Name: "COUNT", Definition: "5N"},
			{Name: "TYP", Definition: "3"},
			{Name: "AMT", Definition: "5"},
			{Name: "OTHER", Definition: "8"},
			{Name: "RIGHT", Definition: "8>"},
		},
	}

	for _, td := range testData {
		t.Run(fmt.Sprintf("with delim '%c'", td.delim), func(t *testing.T) {
			b := &strings.Builder{}
			writer := csv.NewWriter(b)
			writer.Comma = td.delim
			expected := td.expected

			input.WriteHeader(writer)
			writer.Flush()
			actual := b.String()
			if actual != expected {
				t.Logf("expected <> written: %+v <> %+v", expected, actual)
				t.Fail()
			}
		})
	}
}

func TestLTOnRowLexigraphical(t *testing.T) {
	tbl, rowData := basicFixture()
	testData := []struct {
		row      table.Row
		query    []string
		response bool
	}{
		{
			row:      rowData,
			query:    []string{"NAME", "lt", "Charly"},
			response: true,
		},
		{
			row:      rowData,
			query:    []string{"NAME", "lt", "Alice"},
			response: false,
		},
		{
			row:      rowData,
			query:    []string{"NAME", "lt", "bush"},
			response: true,
		},
		{
			row:      rowData,
			query:    []string{"COUNT", "lt", "44"},
			response: false,
		},
		{
			row:      rowData,
			query:    []string{"COUNT", "lt", "444"},
			response: true,
		},
		{
			row:      rowData,
			query:    []string{"COUNT", "lt", "4"},
			response: false,
		},
		{
			row:      rowData,
			query:    []string{"COUNT", "lt", "43"},
			response: false,
		},
		{
			row:      rowData,
			query:    []string{"COUNT", "lt", "45"},
			response: true,
		},
	}

	for _, td := range testData {
		// "Bush", "44", "A", "113", "Another", "This"
		t.Run(fmt.Sprintf("Basic equality tests on multiple columns: %v", td.query), func(t *testing.T) {
			query, err := tbl.Query(td.query)
			if err != nil {
				t.Logf("Expected no error from query, got %v", err)
			}
			match := query(td.row)
			if err != nil {
				t.Logf("Expected no error, got %v", err)
			}
			if match != td.response {

				t.Logf("Expected %+v. got not that", td)
				t.Fail()
			}

		})
	}
}
