package table_test

import (
	"encoding/csv"
	"strings"
	"testing"

	"gitlab.com/yarbelk/grdb/src/errors"
	"gitlab.com/yarbelk/grdb/src/table"
)

func TestFilterTableNames(t *testing.T) {
	input := "NAME\tCOUNT\tTYP\tAMT\tOTHER\tRIGHT\n6\t5N\t3\t5\t8\t8>\nBob\t5\tA\tPink\tNew Worlds\tpasta\n"
	testData := []struct {
		input    *strings.Reader
		filter   []string
		expected string
	}{
		{
			input:    strings.NewReader(input),
			filter:   []string{"NAME", "COUNT"},
			expected: "NAME\tCOUNT\n6\t5N\nBob\t5\n",
		},
		{
			input:    strings.NewReader(input),
			filter:   []string{"NAME"},
			expected: "NAME\n6\nBob\n",
		},
		{
			input:    strings.NewReader(input),
			filter:   []string{"COUNT", "NAME"},
			expected: "COUNT\tNAME\n5N\t6\n5\tBob\n",
		},
		{
			input:    strings.NewReader(input),
			filter:   []string{"TYP", "TYP", "TYP"},
			expected: "TYP\tTYP\tTYP\n3\t3\t3\nA\tA\tA\n",
		},
	}
	for _, td := range testData {
		t.Run("Positive column filters", func(t *testing.T) {
			tbl, err := table.New(td.input, table.Delimiter('\t'))
			b := &strings.Builder{}

			output := csv.NewWriter(b)
			output.Comma = '\t'
			header, err := tbl.HeaderFilterNames(td.filter)
			if err != nil {
				t.Logf("Invalid header input: %+v", err)
				t.FailNow()
			}
			filter, err := tbl.RowFilterNames(td.filter)
			if err != nil {
				t.Logf("Invalid filter input: %+v", err)
				t.FailNow()
			}
			newTable := &table.Table{Header: header}
			newTable.WriteHeader(output)
			row, err := tbl.Read()
			if err != nil {
				t.Logf("Can't read from buffer '%+v'", err)
				t.FailNow()
			}
			output.Write(filter(row))
			output.Flush()
			actual := b.String()
			if actual != td.expected {
				t.Logf("expected <> actual: \n%s\n <>\n%s", td.expected, actual)
				t.FailNow()
			}
		})
	}
}

func TestFilterBadTableNames(t *testing.T) {
	input := "NAME\tCOUNT\tTYP\tAMT\tOTHER\tRIGHT\n6\t5N\t3\t5\t8\t8>\nBob\t5\tA\tPink\tNew Worlds\tpasta\n"
	testData := []struct {
		input  *strings.Reader
		filter []string
		err    error
	}{
		{
			input:  strings.NewReader(input),
			filter: []string{"fake", "COUNT"},
			err:    errors.MissingColumn,
		},
		{
			input:  strings.NewReader(input),
			filter: []string{"COUNT", "FAKE"},
			err:    errors.MissingColumn,
		},
	}
	for _, td := range testData {
		t.Run("Positive column filters", func(t *testing.T) {
			tbl, err := table.New(td.input, table.Delimiter('\t'))

			_, err = tbl.HeaderFilterNames(td.filter)
			if e, ok := err.(errors.GRDBError); !ok || e != errors.MissingColumn {
				t.Logf("expected(%v); got unexpected error for header: %+v", errors.MissingColumn, err)
				t.Fail()
			}
			_, err = tbl.RowFilterNames(td.filter)
			if e, ok := err.(errors.GRDBError); !ok || e != errors.MissingColumn {
				t.Logf("expected(%v); got unexpected error for body: %+v", errors.MissingColumn, err)
				t.Fail()
			}
		})
	}
}
