package table

import (
	"fmt"
	"io"
	"sort"

	"gitlab.com/yarbelk/grdb/src/errors"
)

// SortRows implements the sort.Interface for multi-key stuff
type SortRows struct {
	Rows  []Row
	comps []func(i, j *Row) int
}

func (sr *SortRows) Len() int      { return len(sr.Rows) }
func (sr *SortRows) Swap(i, j int) { sr.Rows[i], sr.Rows[j] = sr.Rows[j], sr.Rows[i] }

func (sr *SortRows) less(p, q *Row) bool {
	for _, comp := range sr.comps {
		switch comp(p, q) {
		case -1:
			// p < q, so we have a decision.
			return true
		case 1:
			// p > q, so we have a decision.
			return false
		case 0:
			continue
		}
	}
	// if the last one is here - its equal, so return false
	return false

}

// Less is from sort examples; its a multi key implementation.
func (sr *SortRows) Less(i, j int) bool {
	p, q := &sr.Rows[i], &sr.Rows[j]
	// Try all but the last comparison.
	return sr.less(p, q)
}

// Sort a table based on the input columns; lexigraphic
// if that column is lexigraphic, numeric if its numeric
// This is stable
func (t *Table) Sort(output Writer, cols []string) error {
	var row Row
	var err error
	sr := &SortRows{}
	sr.comps = make([]func(i, j *Row) int, 0, len(cols))
	for _, c := range cols {
		idx := t.Header.Index(c)
		if idx == -1 {
			return fmt.Errorf("%w: %s", errors.MissingColumn, c)
		}
		sr.comps = append(sr.comps, func(i, j *Row) (k int) {
			k = 1
			// defer func() { log.Println("Comp Func Definition", t.Header, idx, (*i)[idx], (*j)[idx], k) }()

			if (*i)[idx] < (*j)[idx] {
				k = -1
			} else if (*i)[idx] == (*j)[idx] {
				k = 0
			}
			return
		})
	}

loop:
	for {
		row, err = t.reader.Read()
		switch err {
		case io.EOF:
			break loop
		case nil:
		default:
			return err
		}

		sr.Rows = append(sr.Rows, row)

		// i := sort.Search(len(sr.Rows), func(i int) bool {
		// 	for _, comp := range sr.comps {
		// 		switch comp(&sr.Rows[i], &row) {
		// 		case -1:
		// 			return false
		// 		case 0, 1:
		// 			continue
		// 		}
		// 	}
		// 	return true
		// })
		//
		// if i == len(sr.Rows) {
		// 	sr.Rows = append(sr.Rows, row)
		// } else if i == 0 {
		// 	sr.Rows = append([]Row{row}, sr.Rows...)
		//
		// } else {
		// 	// (end up with duplicate row @ i+1, which you overwrite with row
		// 	sr.Rows = append(sr.Rows[:i+1], sr.Rows[i:]...)
		// 	sr.Rows[i] = row
		// }
	}

	sort.Stable(sr)

	for _, row := range sr.Rows {
		err := output.Write(row)
		if err != nil {
			return err
		}
	}

	return nil
}
