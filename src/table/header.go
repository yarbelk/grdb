package table

import (
	"bytes"
	"encoding/csv"
	"io"
)

// ExpandSingleLineHeader takes an input reader and writes the names back to the
// target buffer.  this lets you use whatever was in target again (eg,
// in an io.MultiReader
func ExpandSingleLineHeader(delim rune, rawDefns []string, in io.Reader, target io.Writer) error {
	// using a helper results in too much read from stdin and
	// breaking later steps.  read the first line char by char.
	// since I don't want to deal with messing with unwriting buffers etc
	charbuf := make([]byte, 1)
	line := make([]byte, 0)
	for {
		n, err := in.Read(charbuf)
		if err != nil {
			panic(err)
		}
		if n > 0 {
			if charbuf[0] == '\n' {
				line = append(line, charbuf...)
				break
			}
			if charbuf[0] == '\r' && delim != '\r' {
				continue
			}
			line = append(line, charbuf...)
		}
	}
	csvReader := csv.NewReader(bytes.NewReader(line))
	csvReader.Comma = delim
	names, err := csvReader.Read()

	if err != nil {
		return err
	}

	newHeaderBuff := csv.NewWriter(target)
	newHeaderBuff.Comma = delim
	newHeaderBuff.Write(names)

	defns := make([]string, 0, len(names))

	for i := range names {
		if i >= len(rawDefns) {
			i = len(rawDefns) - 1
		}
		defns = append(defns, rawDefns[i])
	}
	newHeaderBuff.Write(defns)

	newHeaderBuff.Flush()
	return nil
}
