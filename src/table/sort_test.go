package table_test

import (
	"encoding/csv"
	"fmt"
	"strings"
	"testing"

	"gitlab.com/yarbelk/grdb/src/table"
)

func TestSingleColumnSort(t *testing.T) {
	input := "NAME\tCOUNT\tTYP\tAMT\tLEX\tNUM\n" +
		"6\t5\t3\t5\t8\t8\n" +
		"Alice\t5\tA\tZ\t1\t11\n" +
		"Bobby\t5\tB\tX\t111\t111\n" +
		"Charlie\t5\tC\tY\t11\t1\n"
	testData := []struct {
		name     string
		input    *strings.Reader
		columns  []string
		expected string
	}{
		{
			name:     "Single stable sort %s",
			input:    strings.NewReader(input),
			columns:  []string{"COUNT"},
			expected: input,
		},
		{
			name:    "Single lexigraphic numbers %s",
			input:   strings.NewReader(input),
			columns: []string{"LEX"},
			expected: "NAME\tCOUNT\tTYP\tAMT\tLEX\tNUM\n" +
				"6\t5\t3\t5\t8\t8\n" +
				"Alice\t5\tA\tZ\t1\t11\n" +
				"Charlie\t5\tC\tY\t11\t1\n" +
				"Bobby\t5\tB\tX\t111\t111\n",
		},
		{
			name:    "Single lexigraphic words %s",
			input:   strings.NewReader(input),
			columns: []string{"AMT"},
			expected: "NAME\tCOUNT\tTYP\tAMT\tLEX\tNUM\n" +
				"6\t5\t3\t5\t8\t8\n" +
				"Bobby\t5\tB\tX\t111\t111\n" +
				"Charlie\t5\tC\tY\t11\t1\n" +
				"Alice\t5\tA\tZ\t1\t11\n",
		},
	}

	for _, td := range testData {
		t.Run(fmt.Sprintf(td.name, td.columns), func(t *testing.T) {
			newTable, _ := table.New(td.input, table.Delimiter('\t'))
			b := &strings.Builder{}
			output := csv.NewWriter(b)
			output.Comma = '\t'
			newTable.WriteHeader(output)
			err := newTable.Sort(output, td.columns)
			if err != nil {
				t.Logf("Unexpected error: %+v", err)
				t.Fail()
			}
			output.Flush()
			actual := b.String()
			if actual != td.expected {
				t.Logf("actual <> expected:\n\n%s\n\n\t<>\n\n\n%s\n", actual, td.expected)
				t.Fail()
			}
		})
	}
}

func TestMultiColumnSort(t *testing.T) {
	input := "" +
		"NAME\tONE\tTWO\tTHREE\n" +
		"6\t5\t3\t4\n" +
		"Alice\t5\tA\t6\n" +
		"Alice\t5\ta\t5\n" +
		"Bobby\t5\tB\t4\n" +
		"Bobby\t5\tb\t3\n" +
		"Charly\t5\tC\t2\n" +
		"Charly\t5\tc\t1\n"
	testData := []struct {
		name     string
		input    *strings.Reader
		columns  []string
		expected string
	}{
		{
			name:     "multi stable sort %s",
			input:    strings.NewReader(input),
			columns:  []string{"NAME", "ONE"},
			expected: input,
		},
		{
			name:    "multi lexigraphic numbers %s",
			input:   strings.NewReader(input),
			columns: []string{"THREE", "NAME"},
			expected: "" +
				"NAME\tONE\tTWO\tTHREE\n" +
				"6\t5\t3\t4\n" +
				"Charly\t5\tc\t1\n" +
				"Charly\t5\tC\t2\n" +
				"Bobby\t5\tb\t3\n" +
				"Bobby\t5\tB\t4\n" +
				"Alice\t5\ta\t5\n" +
				"Alice\t5\tA\t6\n",
		},
		{
			name:    "Single lexigraphic words %s",
			input:   strings.NewReader(input),
			columns: []string{"NAME", "THREE"},
			expected: "" +
				"NAME\tONE\tTWO\tTHREE\n" +
				"6\t5\t3\t4\n" +
				"Alice\t5\ta\t5\n" +
				"Alice\t5\tA\t6\n" +
				"Bobby\t5\tb\t3\n" +
				"Bobby\t5\tB\t4\n" +
				"Charly\t5\tc\t1\n" +
				"Charly\t5\tC\t2\n",
		},
	}

	for _, td := range testData {
		t.Run(fmt.Sprintf(td.name, td.columns), func(t *testing.T) {
			newTable, _ := table.New(td.input, table.Delimiter('\t'))
			b := &strings.Builder{}
			output := csv.NewWriter(b)
			output.Comma = '\t'
			newTable.WriteHeader(output)
			err := newTable.Sort(output, td.columns)
			if err != nil {
				t.Logf("Unexpected error: %+v", err)
				t.Fail()
			}
			output.Flush()
			actual := b.String()
			if actual != td.expected {
				t.Logf("actual <> expected:\n\n%s\n\n\t<>\n\n\n%s\n", actual, td.expected)
				t.Fail()
			}
		})
	}
}
