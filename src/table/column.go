package table

import "gitlab.com/yarbelk/grdb/src/errors"

// FilterNames will take a table, and only return the columns
// requested, in the order requested
func (t *Table) RowFilterNames(names []string) (func(r Row) Row, error) {
	indexes := make([]int, 0, len(names))

	for _, name := range names {
		index := t.Header.Index(name)
		if index == -1 {
			return nil, errors.MissingColumn
		}
		indexes = append(indexes, index)
	}
	return func(r Row) Row {
		output := make(Row, 0, len(indexes))
		for _, i := range indexes {
			output = append(output, r[i])
		}
		return output
	}, nil
}

func (t *Table) HeaderFilterNames(names []string) (Columns, error) {
	indexes := make([]int, 0, len(names))

	for _, name := range names {
		index := t.Header.Index(name)
		if index == -1 {
			return nil, errors.MissingColumn
		}
		indexes = append(indexes, index)
	}
	output := make(Columns, 0, len(indexes))
	for _, i := range indexes {
		output = append(output, t.Header[i])
	}
	return output, nil
}
