package errors

const (
	MissingColumn       GRDBError = "missing column"
	DuplicateColumn     GRDBError = "duplicate column name"
	WrongArgumentNumber GRDBError = "wrong number of arguments"
	InvalidOperator     GRDBError = "invalid operator"
)

type GRDBError string

func (g GRDBError) Error() string {
	return string(g)
}

func (g GRDBError) Code() int {
	switch g {
	case DuplicateColumn:
		return 10
	case MissingColumn:
		return 20
	case WrongArgumentNumber:
		return 30
	default:
		return 1
	}
}

// FromErr makes basic checks and forces an error to be one that has
// codes for return values.  using the above defined ones if possible
// or just a 'default' exit code of 1 with the bool value
func FromErr(err error) (GRDBError, bool) {
	switch GRDBError(err.Error()) {
	case MissingColumn, DuplicateColumn, WrongArgumentNumber:
		return err.(GRDBError), true
	default:
		return GRDBError(err.Error()), false
	}
}
