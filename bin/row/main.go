package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"

	"gitlab.com/yarbelk/grdb/src/table"
)

var flagSet flag.FlagSet

var help *bool

func init() {

	flagSet = flag.FlagSet{}

	usage := func() {
		fmt.Print(`
Usage: row [options] expression

Currently only supports eq operator, but lets include part of the original help message for fun


Selects rows from the input rdbtable based on an arbitrary expression using column names. Characters that are special to the UNIX shell must be quoted.

Logical constructors 'or' and 'and' may be used; as well as 'null' to indicate empty data values. Comparison operators may be of the form: gt, ge, lt, le, eq, ne, mat, nmat. The first six are the usual operators, E.g 'name eq Hobbs' or 'COUNT gt 100'. The last two stand for 'match' and 'non-match' and are used for pattern matching. They are exactally the same as using the PERL operators '=~' or '!~' respectively, except that pattern matching can be specified easier in expressions, as in:

        NAME  mat   /[Hh]obbs/ 		<<< First letter either case
        NAME  mat   /hobbs/i		<<< any combination of case
        NAME  nmat  /[aeiou]/i		<<< names without vowels

where 'NAME' and 'COUNT' are column names, of course. A warning message is produced on STDERR if either of 'mat' or 'nmat' is used with a numeric type column, but the execution continues. It does not check the '=~' or '!~' forms.

All of the Comparison operators and Logical constructors are reserved and should not be used as column names (they are all lower case and four characters or less).

Since column names and reserved words are parsed by the program, do not put the entire expression in a single pair of quotes as that will prevent the parsing. Also note that column names and reserved words need to be surrounded by blank spaces if they are not individually quoted. For example either form below is fine:

        row   NAME    eq   "L Brown"  <  sample
        row  "NAME"  "eq"  "L Brown"  <  sample

Curently only support the eq operator
`)
		flagSet.PrintDefaults()
	}
	flagSet.Usage = usage

	help = flagSet.Bool("help", false, "print usage")
	flagSet.String("delim", table.DefaultDelimeter, "delimiter to use")
}

func main() {
	if err := flagSet.Parse(os.Args[1:]); err != nil || *help {
		flagSet.Usage()
		if *help {
			os.Exit(0)
		}
		os.Exit(1)
	}
	var delim rune
	if delimVal := flagSet.Lookup("delim"); delimVal != nil && len(delimVal.Value.String()) == 1 {
		delim = rune(delimVal.Value.String()[0])
	} else {
		fmt.Fprintf(os.Stderr, "Invalid delimiter '%s', must be a single rune\n", (delimVal.Value.String()))
		flagSet.Usage()
		os.Exit(1)
	}
	tbl, err := table.New(os.Stdin, table.Delimiter(delim))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid header input: %+v\n", err)
		os.Exit(1)
	}
	query, err := tbl.Query(flagSet.Args())
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid query: %+v\n", err)
		os.Exit(1)
	}

	output := csv.NewWriter(os.Stdout)
	output.Comma = delim

	if err := tbl.WriteHeader(output); err != nil {
		fmt.Fprintf(os.Stderr, "Cannot write header: %+v\n", err)
		os.Exit(1)
	}

	var row table.Row
loop:
	for {
		row, err = tbl.Read()
		switch err {
		case nil:
			break
		case io.EOF:
			break loop
		default:
			fmt.Fprintf(os.Stderr, "Error reading data '%+v'", err)
			output.Flush()
			os.Exit(1)
		}

		if query(row) {
			output.Write(row)
		}
	}
	output.Flush()
}
