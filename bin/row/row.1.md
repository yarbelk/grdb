% ROW(1) Version 0.1
% James Rivett-Carnac
% August 17, 2021

**Usage**: row [options] expresion

SYNOPSYS
========

Select rows from an rdbtable based on their passing of an expression.  Characters that are special to the shell must be quoted.

Expresions have the form **<column name> <op> <value>**


OPTIONS
-------

With

**-help**
:    Print this help information.
**delim**
:     delimiter to use (default "\t")

OPERATORS
---------

The following comparison operators are supported

**eq**
:  column  must equal value

**lt**
:  column  must be lexegraphically less than value

EXAMPLES
--------

All of the Comparison operators and Logical constructors are reserved and
should not be used as column names (they are all lower case and four characters
or less).

Since column names and reserved words are parsed by the program, do not put the entire expression in a single pair of quotes as that will prevent the parsing. Also note that column names and reserved words need to be surrounded by blank spaces if they are not individually quoted. For example either form below is fine:

        row   NAME    eq   "L Brown"  <  sample
        row  "NAME"  "eq"  "L Brown"  <  sample

but do not use this form:

        row  "NAME  eq  L Brown"  <  sample

This operator reads an rdbtable via STDIN and writes an rdbtable via STDOUT.
Options may be abbreviated.

As an example using the sample rdbtable from the DATA section (named sample),
to select rows that have the NAME column equal to 'Hansen' the command would
be:

        row  NAME  eq  Hansen  <  sample

which would produce:

        NAME    COUNT   TYP     AMT     OTHER   RIGHT
        6       5N      4       5N      8       8>
        Hansen  44      A       23      One     Is

