% SORTTBL(1) Version 0.1
% James Rivett-Carnac
% August 5, 2021

**Usage**: sorttbl [options] column [column] ...

SYNOPSYS
========

Sorts an rdbtable on one or more columns. Each column may be sorted in normal (ascending) order.


DESCRIPTION
===========

This operator reads a rdbtable via STDIN and writes a rdbtable via STDOUT. Options may be abbreviated.

OPTIONS
-------

**-help**
:    Print this help information.

**column**
:    A column to sort on.  must be unique
**delim**
:     delimiter to use (default "\t")


EXAMPLES
--------

For example, using the sample data file from the DATA section (named sample) in the following command:

    sorttbl  COUNT  TYP  <  sample

would produce:

    NAME    COUNT   TYP     AMT     OTHER   RIGHT
    6       5N      4       5N      8       8>
    Bush    44      A       133     Another This
    Hansen  44      A       23      One     Is
    Holmes  65      D       1111    On      Edge
    Perry   77      B       244     And     The
    Hart    77      D       1111    So      Right
    Jones   77      X       77      Here    On

