package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"

	"gitlab.com/yarbelk/grdb/src/table"
)

var flagSet flag.FlagSet

var help *bool

func init() {

	flagSet = flag.FlagSet{}

	usage := func() {
		fmt.Print(`
Usage: sorttbl [options] column [column] ...

Sorts an rdbtable on one or more columns. Each column may be sorted in normal (ascending) order.

This operator reads an rdbtable via STDIN and writes an rdbtable via STDOUT. Options may be abbreviated.

Options:

`)

		flagSet.PrintDefaults()

		fmt.Print(`
For example, using the sample data file from the DATA section (named sample) in the following command:

        sorttbl  COUNT  TYP  <  sample

would produce:

        NAME    COUNT   TYP     AMT     OTHER   RIGHT
        6       5N      4       5N      8       8>
        Bush    44      A       133     Another This
        Hansen  44      A       23      One     Is
        Holmes  65      D       1111    On      Edge
        Perry   77      B       244     And     The
        Hart    77      D       1111    So      Right
        Jones   77      X       77      Here    On

`)
	}

	flagSet.Usage = usage

	help = flagSet.Bool("help", false, "print usage")
	flagSet.String("delim", table.DefaultDelimeter, "delimiter to use")
}

func main() {

	if err := flagSet.Parse(os.Args[1:]); err != nil || *help {
		flagSet.Usage()
		if *help {
			os.Exit(0)
		}
		os.Exit(1)
	}
	var delim rune
	if delimVal := flagSet.Lookup("delim"); delimVal != nil && len(delimVal.Value.String()) == 1 {
		delim = rune(delimVal.Value.String()[0])
	} else {
		fmt.Fprintf(os.Stderr, "Invalid delimiter '%s', must be a single rune\n", (delimVal.Value.String()))
		flagSet.Usage()
		os.Exit(1)
	}
	tbl, err := table.New(os.Stdin, table.Delimiter(delim))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid header input: %+v\n", err)
		os.Exit(1)
	}
	output := csv.NewWriter(os.Stdout)
	output.Comma = delim

	if err := tbl.WriteHeader(output); err != nil {
		fmt.Fprintf(os.Stderr, "Cannot write header: %+v\n", err)
		os.Exit(1)
	}
	err = tbl.Sort(output, flagSet.Args())

	output.Flush()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error in sort: %+v", err)
		os.Exit(1)
	}
}
