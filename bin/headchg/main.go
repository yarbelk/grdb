package main

import (
	"bytes"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"

	"gitlab.com/yarbelk/grdb/src/table"
)

var flagSet flag.FlagSet

var help *bool
var single *bool

func init() {

	flagSet = flag.FlagSet{}

	usage := func() {
		fmt.Print(`

Usage: headchg [options]
Options:
`)
		flagSet.PrintDefaults()

		fmt.Print(`
An example is pending

`)
		flagSet.PrintDefaults()
	}
	flagSet.Usage = usage

	help = flagSet.Bool("help", false, "print usage")
	single = flagSet.Bool("single", false, "add [definitions] to the file; as a new second row any missing columns will have the last defn repeated")
	flagSet.String("delim", table.DefaultDelimeter, "delimiter to use")
}

func main() {
	if err := flagSet.Parse(os.Args[1:]); err != nil || *help {
		flagSet.Usage()
		if *help {
			os.Exit(0)
		}
		os.Exit(1)
	}
	var delim rune
	if delimVal := flagSet.Lookup("delim"); delimVal != nil && len(delimVal.Value.String()) == 1 {
		delim = rune(delimVal.Value.String()[0])
	} else {
		fmt.Fprintf(os.Stderr, "Invalid delimiter '%s', must be a single rune\n", (delimVal.Value.String()))
		flagSet.Usage()
		os.Exit(1)
	}

	if !*single {
		fmt.Fprintln(os.Stderr, "Only single is supported right now")
		flagSet.Usage()
		os.Exit(1)
	}

	b := &bytes.Buffer{}
	rawDefns := flagSet.Args()
	if err := table.ExpandSingleLineHeader(delim, rawDefns, os.Stdin, b); err != nil {
		fmt.Fprintf(os.Stderr, "Invalid header input: %+v\n", err)
		flagSet.Usage()
		os.Exit(1)
	}
	input := io.MultiReader(b, os.Stdin)

	tbl, err := table.New(input, table.Delimiter(delim))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid header input: %+v\n", err)
		os.Exit(1)
	}

	output := csv.NewWriter(os.Stdout)
	output.Comma = delim

	if err := tbl.WriteHeader(output); err != nil {
		fmt.Fprintf(os.Stderr, "Cannot write header: %+v\n", err)
		os.Exit(1)
	}

	var row table.Row
loop:
	for {
		row, err = tbl.Read()
		switch err {
		case nil:
			break
		case io.EOF:
			break loop
		default:
			fmt.Fprintf(os.Stderr, "Error reading data '%+v'", err)
			output.Flush()
			os.Exit(1)
		}

		output.Write(row)
	}
	output.Flush()
}
