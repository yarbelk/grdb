% headchg(1) Version 0.1
% James Rivett-Carnac
% August 17, 2021

**Usage**: headchg [options] 

SYNOPSYS
========

Select rows from an rdbtable based on their passing of an expression.  Characters that are special to the shell must be quoted.

Expresions have the form **<column name> <op> <value>**


OPTIONS
-------

With

**-help**
:    Print this help information.
**delim**
:     delimiter to use (default "\t")
**single**
:     Convert a standard single headed TSV/CSV to a rdbtable with a definition row
      In this mode, the [args] will be the ordered definition values, with the last value being repeated

EXAMPLES
--------

with the input

        NAME    COUNT   TYP     AMT     OTHER   RIGHT
        Hansen  44      A       23      One     Is

then the command 

        headchg -single 3 4 5 < sample

would result in

        NAME    COUNT   TYP     AMT     OTHER   RIGHT
        3       4       5       5       5       5
        Hansen  44      A       23      One     Is
