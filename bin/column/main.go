package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"

	"gitlab.com/yarbelk/grdb/src/table"
)

var flagSet flag.FlagSet

var help *bool

func init() {

	flagSet = flag.FlagSet{}

	usage := func() {
		fmt.Print(`
Usage: column [options] list
Selects columns by name (and order) and outputs an rdbtable with these columns. Can effectively select, order, add, delete, or duplicate columns.

The value 'list' is normally a list of column names. If 'list' contains a triplicate of the form '-c NAME NEW' then column name 'NAME' will be changed to 'NEW'. If 'list' contains a triplicate of the form '-a NAME DEFN' then a new (null) column is added, at that point in the list of column names, with name 'NAME' and definition 'DEFN'.

This RDB operator reads an rdbtable from STDIN and writes an rdbtable to STDOUT. Options may be abbreviated.

Options:
`)
		flagSet.PrintDefaults()

		fmt.Print(`
As an example using the sample rdbtable from the DATA section (named sample), to select columns named 'NAME' and 'COUNT' the command would be:

        column  NAME  COUNT  <  sample
To select all columns except column 'NAME' the command would be:
        column  -v  NAME  <  sample
To add a new column named 'LENGTH' with a size of 10 the command would be:
        column  -v  -a  LENGTH  10  <  sample
Note that to include documentation with the new column definition the command would be:
        column  -v  -a  LENGTH  '10 length in meters'  <  sample
The '10 length in meters' must be quoted so that it will be treated as a single token.

Curently only support the basic filtering
`)
	}
	flagSet.Usage = usage

	help = flagSet.Bool("help", false, "print usage")
	flagSet.String("delim", table.DefaultDelimeter, "delimiter to use")
}

func main() {
	if err := flagSet.Parse(os.Args[1:]); err != nil || *help {
		flagSet.Usage()
		if *help {
			os.Exit(0)
		}
		os.Exit(1)
	}
	var delim rune
	if delimVal := flagSet.Lookup("delim"); delimVal != nil && len(delimVal.Value.String()) == 1 {
		delim = rune(delimVal.Value.String()[0])
	} else {
		fmt.Fprintf(os.Stderr, "Invalid delimiter '%s', must be a single rune\n", (delimVal.Value.String()))
		flagSet.Usage()
		os.Exit(1)
	}
	tbl, err := table.New(os.Stdin, table.Delimiter(delim))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid header input: %+v\n", err)
		os.Exit(1)
	}
	header, err := tbl.HeaderFilterNames(flagSet.Args())
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid header input: %+v\n", err)
		os.Exit(1)
	}
	filter, err := tbl.RowFilterNames(flagSet.Args())
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid filter input: %+v\n", err)
		os.Exit(1)
	}

	newTable := &table.Table{Header: header}

	output := csv.NewWriter(os.Stdout)
	output.Comma = delim

	newTable.WriteHeader(output)

	var row table.Row
loop:
	for {
		row, err = tbl.Read()
		switch err {
		case nil:
			break
		case io.EOF:
			break loop
		default:
			fmt.Fprintf(os.Stderr, "Error reading data '%+v'", err)
			output.Flush()
			os.Exit(1)
		}

		output.Write(filter(row))
	}
	output.Flush()
}
