% COLUMN(1) Version 0.1
% James Rivett-Carnac
% August 17, 2021

**Usage**: column [options] column [column] ...

SYNOPSYS
========
Selects columns by name (and order) and outputs an rdbtable with these
columns. Can effectively select, order, add, delete, or duplicate columns.

The value 'list' is normally a list of column names.

This RDB operator reads an rdbtable from STDIN and writes an rdbtable to
STDOUT. Options may be abbreviated.

OPTIONS
-------

With

**-help**
:    Print this help information.
**delim**
:     delimiter to use (default "\t")


EXAMPLES
--------

As an example using the sample rdbtable from the DATA section (named sample),
to select columns named 'NAME' and 'COUNT' the command would be:

        column  NAME  COUNT  <  sample
To select all columns except column 'NAME' the command would be:
        column  -v  NAME  <  sample
To add a new column named 'LENGTH' with a size of 10 the command would be:

BUGS
----

Duplicate columns break other commands and should be removed
